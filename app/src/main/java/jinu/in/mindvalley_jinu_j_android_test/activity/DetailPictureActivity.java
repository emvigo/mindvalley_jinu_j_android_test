package jinu.in.mindvalley_jinu_j_android_test.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.doodle.android.chips.ChipsView;
import com.doodle.android.chips.model.Contact;

import java.util.Date;

import jinu.in.mindvalley_jinu_j_android_test.R;
import jinu.in.mindvalley_jinu_j_android_test.model.Category;
import jinu.in.mindvalley_jinu_j_android_test.model.GridItem;
import jinu.in.mindvalleycache.network.VolleySingleTon;
import jinu.in.mindvalleycache.utils.FuzzyDateTimeFormatter;

@SuppressWarnings("ConstantConditions")
public class DetailPictureActivity extends AppCompatActivity implements View.OnClickListener {

    private NetworkImageView mContentImageView;
    private GridItem mGridItem;
    private Toolbar mToolBar;
    private FloatingActionButton mFab;
    private NetworkImageView mUserPic;
    private TextView mUserName;
    private AppCompatImageView mLikeView;
    private TextView mLikeCount;
    private TextView mCreatedTime;
    private TextView mViewMyPage;
    private ChipsView mChipView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_picure);
        initModel();
        initView();
        initController();
    }

    private void initModel() {
        mGridItem = getIntent().getExtras().getParcelable(HomePictureActivity.RESPONSE);
    }

    private void initController() {
        mChipView.setFocusable(false);
        mChipView.setFocusableInTouchMode(false);
        mChipView.setClickable(false);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setTitle(mGridItem.getUser().getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContentImageView.setImageUrl(mGridItem.getUrls().getFull(),
                getImageLoader());
        addChips();
        mCreatedTime.setText(getTimeAgo());
        mUserPic.setImageUrl(mGridItem.getUser().getProfileImage().getSmall(),
                getImageLoader());
        mLikeCount.setText(String.valueOf(mGridItem.getLikes()));
        mUserName.setText(String.format(getString(R.string.user_name),
                mGridItem.getUser().getUsername()));
        mViewMyPage.setText(getFormat());
        mLikeView.setImageResource(R.drawable.ic_thumb_up_black_24dp);
        mViewMyPage.setOnClickListener(this);
        mFab.setOnClickListener(this);
    }

    private String getTimeAgo() {
        return String.format(getString(R.string.created_s),
                FuzzyDateTimeFormatter.getTimeAgo(this, new Date(mGridItem.getTime())));
    }

    private String getFormat() {
        return String.format(getString(R.string.view_more_from_s),
                mGridItem.getUser().getName());
    }

    private ImageLoader getImageLoader() {
        return VolleySingleTon.getInstance(this).getImageLoader();
    }

    private void addChips() {

    }

    private void initView() {
        mUserPic = (NetworkImageView) findViewById(R.id.user_pic);
        mUserName = (TextView) findViewById(R.id.user_name);
        mLikeView = (AppCompatImageView) findViewById(R.id.like_view);
        mLikeCount = (TextView) findViewById(R.id.like_count);
        mCreatedTime = (TextView) findViewById(R.id.created_time);
        mViewMyPage = (TextView) findViewById(R.id.view_my_page);
        mChipView = (ChipsView) findViewById(R.id.chip_view);
        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        mContentImageView = (NetworkImageView) findViewById(R.id.content_image_view);
        mFab = (FloatingActionButton) findViewById(R.id.fab);
    }

    @Override
    public void onClick(View v) {
        if (v == mFab) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mGridItem.getLinks().getDownload())));
        } else if (v == mViewMyPage) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mGridItem.getUser().getLinks().getHtml())));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
