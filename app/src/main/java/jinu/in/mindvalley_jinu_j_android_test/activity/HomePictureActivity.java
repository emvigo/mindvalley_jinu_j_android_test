package jinu.in.mindvalley_jinu_j_android_test.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;


import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import jinu.in.mindvalley_jinu_j_android_test.R;
import jinu.in.mindvalley_jinu_j_android_test.adapter.GridAdapter;
import jinu.in.mindvalley_jinu_j_android_test.model.GridList;
import jinu.in.mindvalley_jinu_j_android_test.model.GridItem;
import jinu.in.mindvalley_jinu_j_android_test.utils.SpacesItemDecoration;
import jinu.in.mindvalleycache.model.Response;
import jinu.in.mindvalleycache.network.VolleyServiceWorker;
import jinu.in.mindvalleycache.network.VolleySingleTon;
import jinu.in.mindvalleycache.view.ErrorProgressView;

@SuppressWarnings("ConstantConditions")
public class HomePictureActivity extends AppCompatActivity {

    public static final String RESPONSE = "response";
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    private static final String TAG = "HomePictureActivity";
    private ErrorProgressView mErrorProgressView;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private GridAdapter mGridAdapter;
    private GridList mGridList;
    private Toolbar mToolBar;
    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_picture);
        initModel();
        initView();
        initController();
    }

    /**
     * initializes model classes
     */
    private void initModel() {
        mGridList = new GridList();
        ImageLoader imageLoader = VolleySingleTon.getInstance(this).getImageLoader();
        mGridAdapter = new GridAdapter(mGridList, imageLoader, mListSelectedListener);
        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(2,
                StaggeredGridLayoutManager.VERTICAL);
    }

    /**
     * set values to views and adds listener
     */
    private void initController() {
        mRecyclerView.setLayoutManager(mStaggeredGridLayoutManager);
        SpacesItemDecoration decoration = new SpacesItemDecoration(8, 2);
        mRecyclerView.addItemDecoration(decoration);
        mRecyclerView.setAdapter(mGridAdapter);
        mErrorProgressView.showProgress(true);
        mErrorProgressView.setOnRetryAction(mRetryListener);
        mSwipeRefreshLayout.setOnRefreshListener(mRefreshListener);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setTitle("Test");
        callService();
    }

    /**
     * initializes views
     */
    private void initView() {
        mErrorProgressView = (ErrorProgressView) findViewById(R.id.view);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mToolBar = (Toolbar) findViewById(R.id.tool_bar);
    }

    /**
     * Once retry button is clicked this interface will invoke
     */
    private ErrorProgressView.OnRetryAction mRetryListener = new ErrorProgressView.OnRetryAction() {
        @Override
        public void onRetry() {
            callService();
        }
    };

    /**
     * calls web service from server
     */
    private void callService() {
        showProgress();
        VolleyServiceWorker volleyServiceWorker = new VolleyServiceWorker(mVolleyCallback);
        volleyServiceWorker.setUrl("http://pastebin.com/raw/wgkJgazE");
        volleyServiceWorker.setMethod(Request.Method.GET);
        volleyServiceWorker.saveInCache(true);
        volleyServiceWorker.execute(this);
    }

    /**
     * shows progress if no item error progress will show progress view
     * otherwise swipeRefresh progress is shown
     */
    private void showProgress() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mErrorProgressView.showProgress(false);
        } else {
            mErrorProgressView.showProgress(true);
        }
    }

    /**
     * swipe refresh on list will invoke onRefresh interface
     */
    private SwipeRefreshLayout.OnRefreshListener mRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            callService();
        }
    };

    /**
     * once service is called data will be refreshed
     * if success returns result on onResponse otherwise returns
     * onError interface
     */
    private VolleyServiceWorker.VolleyCallback mVolleyCallback = new VolleyServiceWorker.VolleyCallback() {
        @Override
        public void onResponse(Response response) {
            dismissProgress();
            parseResponse(response);

        }

        @Override
        public void onError(VolleyError volleyError, String url) {
            dismissProgress();
            mErrorProgressView.setErrorType(ErrorProgressView.TYPE_NETWORK);
            Log.d(TAG, "onError() called with: volleyError = [" + volleyError + "], url = [" + url + "]");
        }
    };

    /**
     * if circle progress is shows it dismisses else
     * SwipeRefresh progress will be dismissed
     */
    private void dismissProgress() {
        mErrorProgressView.showProgress(false);
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @SuppressWarnings("unchecked")
    private void parseResponse(Response response) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<GridItem>>() {
        }.getType();

        mGridList.setGridItems((ArrayList<GridItem>) gson.fromJson(response.result, listType));
        formatTime();
        notifyAdapter();
    }

    /**
     * notifies list adapter in about 100 ms
     */
    private void notifyAdapter() {
        mErrorProgressView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mGridAdapter.notifyDataSetChanged();
            }
        }, 100);
    }

    /**
     * formats time to long variable
     */
    private void formatTime() {
        try {
            for (GridItem gridItem : mGridList.getGridItems()) {
                if (TextUtils.isEmpty(gridItem.getCreatedAt())) {
                    continue;
                }
                String time = gridItem.getCreatedAt().split("T")[0] + " " + gridItem.getCreatedAt().split("T")[1].split("-")[0];
                gridItem.setTime(dateFormat.parse(time).getTime());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(RESPONSE, mGridList);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState == null) {
            mGridList = savedInstanceState.getParcelable(RESPONSE);
        }
    }


    /**
     * this calls when user selects an item
     */
    private GridAdapter.OnListSelected mListSelectedListener = new GridAdapter.OnListSelected() {
        @Override
        public void onClick(int pos) {
            Intent intent = new Intent(HomePictureActivity.this, DetailPictureActivity.class);
            intent.putExtra(RESPONSE, mGridList.getGridItems().get(pos));
            startActivity(intent);
        }
    };
}
