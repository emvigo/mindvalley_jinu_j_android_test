package jinu.in.mindvalley_jinu_j_android_test.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.Date;

import jinu.in.mindvalley_jinu_j_android_test.R;
import jinu.in.mindvalley_jinu_j_android_test.model.GridList;
import jinu.in.mindvalley_jinu_j_android_test.model.GridItem;
import jinu.in.mindvalleycache.utils.FuzzyDateTimeFormatter;
import jinu.in.mindvalleycache.views.RoundedNetworkImageView;

/**
 * Created by jinu on 7/28/2016.
 **/

public class GridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private GridList mGridList;
    private ImageLoader mImageLoader;

    private static final int list_normal_h = 0;
    private static final int list_large_h = 1;
    private static final int list_huge_h = 2;
    private static final int list_normal_w = 3;
    private int lastPosition = 0;

    private OnListSelected mOnListSelected;

    public interface OnListSelected {
        void onClick(int pos);
    }

    public GridAdapter(GridList mGridList, ImageLoader imageLoader, OnListSelected mOnListSelected) {

        this.mGridList = mGridList;
        this.mImageLoader = imageLoader;
        this.mOnListSelected = mOnListSelected;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list, null);
        if (viewType == list_normal_h) {
            return new GridAdapterNormalVH(view, mOnListSelected);
        } else if (viewType == list_large_h) {
            return new GridAdapterLargeHeightVH(view, mOnListSelected);
        } else if (viewType == list_huge_h) {
            return new GridAdapterLargeHeightHugeVH(view, mOnListSelected);
        } else {
            return new GridAdapterLargeWidthVH(view, mOnListSelected);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder gridAdapterVH, int position) {
        GridAdapterVH holder = (GridAdapterVH) gridAdapterVH;
        GridItem gridItem = getPinList(position);
        holder.mUserView.setImageUrl(gridItem.getUser().getProfileImage().getSmall(), mImageLoader);
        holder.mCategory.setText(getCategory(gridItem));
        holder.mUserInfo.setText(gridItem.getUser().getName());
        holder.mContainerLoader.setImageUrl(gridItem.getUrls().getSmall(), mImageLoader);
        holder.mTime.setText(FuzzyDateTimeFormatter.getTimeAgo(getContext(holder),
                new Date(gridItem.getTime())));
        holder.mLikeTxt.setText(Html.fromHtml(
                String.format(getContext(holder).getString(R.string.like_text),
                        gridItem.getLikes())));
        showAnimation(position, holder);
    }

    private void showAnimation(int position, GridAdapterVH holder) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(getContext(holder),
                    R.anim.translate_from_top);
            holder.itemView.startAnimation(animation);
        }
        lastPosition = holder.getAdapterPosition();
    }

    /**
     * method to return context from viewHolder
     *
     * @param holder holder
     * @return context
     */
    private Context getContext(GridAdapterVH holder) {
        return holder.mTime.getContext();
    }

    /**
     * method parses category from arrayList and joins with comma
     *
     * @param gridItem gridItem
     * @return comma separated categoryList
     */
    private String getCategory(GridItem gridItem) {
        if (gridItem.getCategories() == null && gridItem.getCategories().isEmpty()) {
            return "nil";
        }
        String[] category = new String[gridItem.getCategories().size()];
        for (int i = 0; i < gridItem.getCategories().size(); i++) {
            category[i] = gridItem.getCategories().get(i).getTitle();
        }
        return TextUtils.join(",", category);
    }

    /**
     * method to get list item from pos
     *
     * @param position position
     * @return GridItem
     */
    private GridItem getPinList(int position) {
        return mGridList.getGridItems().get(position);
    }

    @Override
    public int getItemCount() {
        if (mGridList == null) {
            return 0;
        }
        if (mGridList.getGridItems() == null) {
            return 0;
        }
        return mGridList.getGridItems().size();
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemViewType(int position) {
        if (getPinList(position).getHeight() > 2000) {
            return list_large_h;
        }
        if (getPinList(position).getHeight() >= 2500) {
            return list_huge_h;
        }
        if (getPinList(position).getWidth() > 4000) {
            return list_normal_w;
        }
        return list_normal_h;
    }

    /**
     * superclass of all viewHolders
     */
    static abstract class GridAdapterVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        NetworkImageView mContainerLoader;
        TextView mLikeTxt;
        TextView mUserInfo;
        TextView mCategory;
        RoundedNetworkImageView mUserView;
        TextView mTime;
        private OnListSelected mOnListSelected;
        private View itemView;

        GridAdapterVH(View itemView, OnListSelected mOnListSelected) {
            super(itemView);
            this.itemView = itemView;
            mContainerLoader = (NetworkImageView) itemView.findViewById(R.id.main_view);
            mLikeTxt = (TextView) itemView.findViewById(R.id.some_info);
            mUserInfo = (TextView) itemView.findViewById(R.id.user_name);
            mCategory = (TextView) itemView.findViewById(R.id.info_data);
            mUserView = (RoundedNetworkImageView) itemView.findViewById(R.id.user_pic);
            mTime = (TextView) itemView.findViewById(R.id.time_formatted);
            this.mOnListSelected = mOnListSelected;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnListSelected != null) {
                mOnListSelected.onClick(getAdapterPosition());
            }
        }
    }

    private static class GridAdapterNormalVH extends GridAdapterVH {


        GridAdapterNormalVH(View itemView, OnListSelected mOnListSelected) {
            super(itemView, mOnListSelected);
        }

    }


    private static class GridAdapterLargeHeightVH extends GridAdapterVH {


        GridAdapterLargeHeightVH(View itemView, OnListSelected mOnListSelected) {
            super(itemView, mOnListSelected);
            LinearLayout.LayoutParams layoutParams;
            layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 250);
            mContainerLoader.setLayoutParams(layoutParams);
        }


    }

    private static class GridAdapterLargeHeightHugeVH extends GridAdapterVH {


        GridAdapterLargeHeightHugeVH(View itemView, OnListSelected mOnListSelected) {
            super(itemView,mOnListSelected);
            LinearLayout.LayoutParams layoutParams;
            layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 325);
            mContainerLoader.setLayoutParams(layoutParams);
        }

    }


    private static class GridAdapterLargeWidthVH extends GridAdapterVH {

        GridAdapterLargeWidthVH(View itemView, OnListSelected mOnListSelected) {
            super(itemView,mOnListSelected);
            LinearLayout.LayoutParams layoutParams;
            layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 160);
            mContainerLoader.setLayoutParams(layoutParams);
        }

    }
}
