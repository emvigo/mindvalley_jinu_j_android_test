
package jinu.in.mindvalley_jinu_j_android_test.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Links__ implements Parcelable {

    @SerializedName("self")
    @Expose
    private String self;
    @SerializedName("html")
    @Expose
    private String html;
    @SerializedName("download")
    @Expose
    private String download;
    public final static Parcelable.Creator<Links__> CREATOR = new Creator<Links__>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Links__ createFromParcel(Parcel in) {
            Links__ instance = new Links__();
            instance.self = ((String) in.readValue((String.class.getClassLoader())));
            instance.html = ((String) in.readValue((String.class.getClassLoader())));
            instance.download = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Links__[] newArray(int size) {
            return (new Links__[size]);
        }

    };

    /**
     * @return The self
     */
    public String getSelf() {
        return self;
    }

    /**
     * @param self The self
     */
    public void setSelf(String self) {
        this.self = self;
    }

    /**
     * @return The html
     */
    public String getHtml() {
        return html;
    }

    /**
     * @param html The html
     */
    public void setHtml(String html) {
        this.html = html;
    }

    /**
     * @return The download
     */
    public String getDownload() {
        return download;
    }

    /**
     * @param download The download
     */
    public void setDownload(String download) {
        this.download = download;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(self);
        dest.writeValue(html);
        dest.writeValue(download);
    }

    public int describeContents() {
        return 0;
    }

}
