
package jinu.in.mindvalley_jinu_j_android_test.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class GridItem implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("width")
    @Expose
    private int width;
    @SerializedName("height")
    @Expose
    private int height;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("likes")
    @Expose
    private int likes;
    @SerializedName("liked_by_user")
    @Expose
    private boolean likedByUser;

    @SerializedName("time")
    @Expose
    private long time;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("current_user_collections")
    @Expose

    private List<Object> currentUserCollections = new ArrayList<>();
    @SerializedName("urls")
    @Expose

    private Urls urls;
    @SerializedName("categories")
    @Expose

    private List<Category> categories = new ArrayList<>();
    @SerializedName("links")
    @Expose

    private Links__ links;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width The width
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return The height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height The height
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return The color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color The color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return The likes
     */
    public int getLikes() {
        return likes;
    }

    /**
     * @param likes The likes
     */
    public void setLikes(int likes) {
        this.likes = likes;
    }

    /**
     * @return The likedByUser
     */
    public boolean isLikedByUser() {
        return likedByUser;
    }

    /**
     * @param likedByUser The liked_by_user
     */
    public void setLikedByUser(boolean likedByUser) {
        this.likedByUser = likedByUser;
    }

    /**
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return The currentUserCollections
     */
    public List<Object> getCurrentUserCollections() {
        return currentUserCollections;
    }

    /**
     * @param currentUserCollections The current_user_collections
     */
    public void setCurrentUserCollections(List<Object> currentUserCollections) {
        this.currentUserCollections = currentUserCollections;
    }

    /**
     * @return The urls
     */
    public Urls getUrls() {
        return urls;
    }

    /**
     * @param urls The urls
     */
    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    /**
     * @return The categories
     */
    public List<Category> getCategories() {
        return categories;
    }

    /**
     * @param categories The categories
     */
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    /**
     * @return The links
     */
    public Links__ getLinks() {
        return links;
    }

    /**
     * @param links The links
     */
    public void setLinks(Links__ links) {
        this.links = links;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.createdAt);
        dest.writeInt(this.width);
        dest.writeInt(this.height);
        dest.writeString(this.color);
        dest.writeInt(this.likes);
        dest.writeByte(this.likedByUser ? (byte) 1 : (byte) 0);
        dest.writeLong(this.time);
        dest.writeParcelable(this.user, flags);
        dest.writeList(this.currentUserCollections);
        dest.writeParcelable(this.urls, flags);
        dest.writeTypedList(this.categories);
        dest.writeParcelable(this.links, flags);
    }

    public GridItem() {
    }

    protected GridItem(Parcel in) {
        this.id = in.readString();
        this.createdAt = in.readString();
        this.width = in.readInt();
        this.height = in.readInt();
        this.color = in.readString();
        this.likes = in.readInt();
        this.likedByUser = in.readByte() != 0;
        this.time = in.readLong();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.currentUserCollections = new ArrayList<Object>();
        in.readList(this.currentUserCollections, Object.class.getClassLoader());
        this.urls = in.readParcelable(Urls.class.getClassLoader());
        this.categories = in.createTypedArrayList(Category.CREATOR);
        this.links = in.readParcelable(Links__.class.getClassLoader());
    }

    public static final Creator<GridItem> CREATOR = new Creator<GridItem>() {
        @Override
        public GridItem createFromParcel(Parcel source) {
            return new GridItem(source);
        }

        @Override
        public GridItem[] newArray(int size) {
            return new GridItem[size];
        }
    };
}
