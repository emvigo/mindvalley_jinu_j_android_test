package jinu.in.mindvalley_jinu_j_android_test.model;

import android.os.Parcel;

import java.util.ArrayList;

import jinu.in.mindvalleycache.model.Response;

/**
 * Created by jinu on 7/29/2016.
 **/

public class GridList extends Response{

    private ArrayList<GridItem> gridItems;

    public ArrayList<GridItem> getGridItems() {
        return gridItems;
    }

    public void setGridItems(ArrayList<GridItem> gridItems) {
        this.gridItems = gridItems;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.gridItems);
    }

    public GridList() {
    }

    protected GridList(Parcel in) {
        super(in);
        this.gridItems = in.createTypedArrayList(GridItem.CREATOR);
    }

    public static final Creator<GridList> CREATOR = new Creator<GridList>() {
        @Override
        public GridList createFromParcel(Parcel source) {
            return new GridList(source);
        }

        @Override
        public GridList[] newArray(int size) {
            return new GridList[size];
        }
    };
}
