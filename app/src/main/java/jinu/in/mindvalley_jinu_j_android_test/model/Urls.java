
package jinu.in.mindvalley_jinu_j_android_test.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Urls implements Parcelable {

    @SerializedName("raw")
    @Expose
    private String raw;
    @SerializedName("full")
    @Expose
    private String full;
    @SerializedName("regular")
    @Expose
    private String regular;
    @SerializedName("small")
    @Expose
    private String small;
    @SerializedName("thumb")
    @Expose
    private String thumb;
    public final static Parcelable.Creator<Urls> CREATOR = new Creator<Urls>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Urls createFromParcel(Parcel in) {
            Urls instance = new Urls();
            instance.raw = ((String) in.readValue((String.class.getClassLoader())));
            instance.full = ((String) in.readValue((String.class.getClassLoader())));
            instance.regular = ((String) in.readValue((String.class.getClassLoader())));
            instance.small = ((String) in.readValue((String.class.getClassLoader())));
            instance.thumb = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Urls[] newArray(int size) {
            return (new Urls[size]);
        }

    };

    /**
     * @return The raw
     */
    public String getRaw() {
        return raw;
    }

    /**
     * @param raw The raw
     */
    public void setRaw(String raw) {
        this.raw = raw;
    }

    /**
     * @return The full
     */
    public String getFull() {
        return full;
    }

    /**
     * @param full The full
     */
    public void setFull(String full) {
        this.full = full;
    }

    /**
     * @return The regular
     */
    public String getRegular() {
        return regular;
    }

    /**
     * @param regular The regular
     */
    public void setRegular(String regular) {
        this.regular = regular;
    }

    /**
     * @return The small
     */
    public String getSmall() {
        return small;
    }

    /**
     * @param small The small
     */
    public void setSmall(String small) {
        this.small = small;
    }

    /**
     * @return The thumb
     */
    public String getThumb() {
        return thumb;
    }

    /**
     * @param thumb The thumb
     */
    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(raw);
        dest.writeValue(full);
        dest.writeValue(regular);
        dest.writeValue(small);
        dest.writeValue(thumb);
    }

    public int describeContents() {
        return 0;
    }

}
