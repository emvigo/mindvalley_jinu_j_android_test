package jinu.in.mindvalleycache.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jinu on 7/28/2016.
 **/

public class Response implements Parcelable {

    public boolean isCache=false;
    public String Url;
    public String result;


    public Response() {
    }

    public void setResult(String result){
        this.result = result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isCache ? (byte) 1 : (byte) 0);
        dest.writeString(this.Url);
        dest.writeString(this.result);
    }

    protected Response(Parcel in) {
        this.isCache = in.readByte() != 0;
        this.Url = in.readString();
        this.result = in.readString();
    }

    public static final Creator<Response> CREATOR = new Creator<Response>() {
        @Override
        public Response createFromParcel(Parcel source) {
            return new Response(source);
        }

        @Override
        public Response[] newArray(int size) {
            return new Response[size];
        }
    };
}
