package jinu.in.mindvalleycache.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;


import jinu.in.mindvalleycache.model.Response;

/**
 * Created by jinu on 7/28/2016.
 **/

public class VolleyServiceWorker {

    private static final String TAG = "VolleyServiceWorker";
    private VolleyCallback mVolleyCallback;
    private String url;
    private int method = Request.Method.GET;
    private boolean saveCache;


    /**
     * sets  url
     *
     * @param url of API
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @param method type default will be GET
     */
    public void setMethod(int method) {
        this.method = method;
    }

    /**
     * @param saveCache if true saves data in cache default value is false
     */
    public void saveInCache(boolean saveCache) {
        this.saveCache = saveCache;
    }


    /**
     * this interface delivers response
     */
    public interface VolleyCallback {

        /**
         * method returns of any data is fetched from network
         *
         * @param response model class
         */
        void onResponse(Response response);

        /**
         * @param volleyError error type of response
         * @param url         identification if has same listener is used
         */
        void onError(VolleyError volleyError, String url);

    }

    /**
     * @param mVolleyCallback interface of service worker
     */
    public VolleyServiceWorker(VolleyCallback mVolleyCallback) {
        if (mVolleyCallback == null) {
            throw new NullPointerException("Please implement VolleyCallback");
        }
        this.mVolleyCallback = mVolleyCallback;
    }

    /**
     * execute query
     *
     * @param context context
     */
    public void execute(Context context) {
        if (url == null) {
            throw new NullPointerException("Please Specify url");
        }
        if (context==null){
            throw new NullPointerException("Context is empty");
        }

        StringRequestExt stringRequest = new StringRequestExt(method, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse() called with: response = [" + response + "]");
                sendResponse(response);
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mVolleyCallback != null) {
                    mVolleyCallback.onError(error, url);
                }
            }
        }, saveCache);
        VolleySingleTon.getInstance(context).addToRequestQueue(stringRequest);
    }

    /**
     * init  model class and deliver response
     *
     * @param response response
     */
    private void sendResponse(String response) {
        if (mVolleyCallback != null) {
            Response response1 = new Response();
            response1.setResult(response);
            response1.Url = url;
            response1.isCache = saveCache;
            mVolleyCallback.onResponse(response1);
        }
    }
}
